from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import ToDo
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from .forms import ToDoForm
from django.contrib.auth import logout
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.models import User

# Create your views here.

def todo_list_ids(request):
    todos = ToDo.objects.values_list('id', flat=True)
    return render(request, 'todos/todo_list_ids.html', {'todos': todos})

def todo_list_ids_titles(request):
    todos = ToDo.objects.values('id', 'title')
    return render(request, 'todos/todo_list_ids_titles.html', {'todos': todos})

def todo_list_unresolved(request):
    todos = ToDo.objects.filter(is_resolved=False).values('id', 'title')
    return render(request, 'todos/todo_list_unresolved.html', {'todos': todos})

def todo_list_resolved(request):
    todos = ToDo.objects.filter(is_resolved=True).values('id', 'title')
    return render(request, 'todos/todo_list_resolved.html', {'todos': todos})

def todo_list_ids_userids(request):
    todos = ToDo.objects.values('id', 'user_id')
    return render(request, 'todos/todo_list_ids_userids.html', {'todos': todos})

def todo_list_resolved_userids(request):
    todos = ToDo.objects.filter(is_resolved=True).values('id', 'user_id')
    return render(request, 'todos/todo_list_resolved_userids.html', {'todos': todos})

def todo_list_unresolved_userids(request):
    todos = ToDo.objects.filter(is_resolved=False).values('id', 'user_id')
    return render(request, 'todos/todo_list_unresolved_userids.html', {'todos': todos})


def home(request):
    unresolved_tasks = list(ToDo.objects.filter(is_resolved=False).values('id', 'title'))
    resolved_tasks = list(ToDo.objects.filter(is_resolved=True).values('title'))

    context = {
        'unresolved_tasks': unresolved_tasks,
        'resolved_tasks': resolved_tasks,
    }
    return render(request, 'todos/home.html', context)

def create_task(request):
    if request.method == 'POST':
        form = ToDoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')  
        else:
            messages.error(request, 'Error al crear la tarea')
    else:
        form = ToDoForm()

    context = {
        'form': form
    }
    return render(request, 'todos/create_task.html', context)

def delete_task(request, task_id):
    task = get_object_or_404(ToDo, pk=task_id)
    if request.method == 'POST':
        task.delete()
        return redirect('home')

    return render(request, 'todos/delete_task.html', {'task': task})

def update_task(request, task_id):
    task = get_object_or_404(ToDo, id=task_id)
    users = User.objects.all()
    if request.method == 'POST':
        form = ToDoForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = ToDoForm(instance=task)

    context = {
        'form': form,
        'task': task,
        'users': users,
    }
    return render(request, 'todos/update_task.html', context)


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'La cuenta de {username} ha sido creada. Ahora puedes iniciar sesión')
            return redirect('login')  
    else:
        form = UserRegisterForm()
    return render(request, 'todos/register.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f'Has iniciado sesión como {username}')
                return redirect('home')  
            else:
                messages.error(request, 'Usuario o contraseña incorrectos')
        else:
            messages.error(request, 'Usuario o contraseña incorrectos')
    else:
        form = AuthenticationForm()
    return render(request, 'todos/login.html', {'form': form})

def logout_view(request):
    logout(request)
    return redirect('login')