from django.urls import path
from django.contrib.auth import views as auth_views
from .views import (
    todo_list_ids, todo_list_ids_titles, todo_list_unresolved,
    todo_list_resolved, todo_list_ids_userids, todo_list_resolved_userids,
    todo_list_unresolved_userids,register, login_view,home,create_task, delete_task, update_task,logout_view
)

urlpatterns = [
    path('todos/ids/', todo_list_ids, name='todo-list-ids'),
    path('todos/ids-titles/', todo_list_ids_titles, name='todo-list-ids-titles'),
    path('todos/unresolved/', todo_list_unresolved, name='todo-list-unresolved'),
    path('todos/resolved/', todo_list_resolved, name='todo-list-resolved'),
    path('todos/ids-userids/', todo_list_ids_userids, name='todo-list-ids-userids'),
    path('todos/resolved-userids/', todo_list_resolved_userids, name='todo-list-resolved-userids'),
    path('todos/unresolved-userids/', todo_list_unresolved_userids, name='todo-list-unresolved-userids'),
    path('register/', register, name='register'),
    path('login/', login_view, name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='home'), name='logout'),
    path('', home, name='home'),
    path('create/', create_task, name='create_task'),
    path('delete/<int:task_id>/', delete_task, name='delete_task'),
    path('update/<int:task_id>/', update_task, name='update_task'),
]
